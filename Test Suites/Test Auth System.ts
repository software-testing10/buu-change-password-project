<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Test Auth System</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>52236501-c8be-4f5f-aa8d-e4986a8f4029</testSuiteGuid>
   <testCaseLink>
      <guid>8818a9aa-3660-450c-8186-a5034b8a46ae</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Auth/Login Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7473dd2c-a890-4d49-9df7-5bab703d8bf1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Auth/Login Fail</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
