<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Test Change Password System</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>eaf1a38d-43cb-4bf3-a582-11acdf21de8e</testSuiteGuid>
   <testCaseLink>
      <guid>9ccb347e-a65c-4eba-a792-b29213016186</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Change Password/Change Password Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>db2a5946-f932-4c97-8b15-5b13abb3b27b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Change Password/Change Password Fail no alphabet</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4fd8abde-6d61-46d9-9a35-fd7da6b0429f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Change Password/Change Password Fail no number</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4277c9d2-adc1-4cb6-9aa6-16d57a1faf09</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Change Password/Change Password Fail no special character</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9f810856-0034-4f2a-a917-eff10912a5dd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Change Password/Change Password Fail Length less than 8</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fe507a48-0c79-43c6-bbd6-855ad7a46059</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Auth/Logout Success</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
